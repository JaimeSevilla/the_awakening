using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    [SerializeField] private float vida;
    private Animator animator;
    public GameObject enemigo;
    void Start()
    {
        animator = GetComponent<Animator>();

    }

    public void TomarDa�o(float da�o)
    {
        vida -= da�o;
        animator.SetTrigger("Golpeado");
        if (vida <= 0)
        {
            Muerte();
        }
    }
    private void Muerte()
    {
        animator.SetTrigger("Muerte");
        Destroy(enemigo);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
