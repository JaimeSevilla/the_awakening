using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField]
    public float Mspeed;
    private bool FacingRight;
    private Animator myanimator;
    public GameObject Hitbox;
    public GameObject Hit;
    public GameObject Player;
    [SerializeField] private float vida;



    private bool attack;
    private bool chargedattack;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        myanimator = GetComponent<Animator>();
    }

    private void Update()
    {

        HandleInput();
    }
    void FixedUpdate()
    {
        Vector3 Thescale = transform.localScale;
        Thescale.z = 0;
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        myanimator.SetFloat("Speed", Mathf.Abs(horizontal));

        Movimiento(horizontal, vertical);
        Flip(horizontal);
        HandleAttack();
        ResetValues();
    }

    private void Movimiento(float horizontal, float vertical)
    {
        if (!this.myanimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack") && !this.myanimator.GetCurrentAnimatorStateInfo(0).IsTag("Charged Attack"))
        {
            rb.velocity = new Vector2(horizontal * Mspeed, 0);
        }
        else if (this.myanimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            rb.velocity = new Vector2(0, 0);
        } else if (this.myanimator.GetCurrentAnimatorStateInfo(0).IsTag("Charged Attack"))
        {
            rb.velocity = new Vector2(0, 0);
        }

    }

    private void Flip(float horizontal)
    {
        if ((horizontal < 0 && !FacingRight) || (horizontal > 0 && FacingRight))
        {
            FacingRight = !FacingRight;
            Vector3 TheScale = transform.localScale;
            TheScale.x *= -1;

            transform.localScale = TheScale;
        }
    }
    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            attack = true;
            CreateHitBox();
        }
    }

    private void HandleAttack()
    {
        if (attack && !this.myanimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            myanimator.SetTrigger("Attack");
        }
    }

    private void ResetValues()
    {
        attack = false;
    }

    public void CreateHitBox()
    {
        if (GameObject.Find("Hit(Clone)"))
        {
            return;
        }
        else
        {
            Vector3 PositionHit = new Vector3(Hitbox.transform.position.x, Hitbox.transform.position.y, 0);
            GameObject temphit = Instantiate(Hit, PositionHit, Quaternion.identity);
            Destroy(temphit, 1);
        }
    }

    public void TomarDa�o(float da�o)
    {
        vida -= da�o;
        myanimator.SetTrigger("Golpeado");
        if (vida < 0)
        {
            Muerte();
        }
    }
    private void Muerte()
    {
        myanimator.SetTrigger("Muerte");
        Destroy(Player);
    }
}
