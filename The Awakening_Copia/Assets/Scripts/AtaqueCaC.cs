using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueCaC : MonoBehaviour
{
    [SerializeField] private Transform controladorGolpe;
    [SerializeField] private float radioGolpe;
    [SerializeField] private float da�oGolpe = 1;

    private void FixedUpdate()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Golpe();
        }
    }
    private void Golpe()
    {
        Collider2D[] objetos = Physics2D.OverlapCircleAll(controladorGolpe.position, radioGolpe);

        foreach (Collider2D colisionador in objetos)
        {
            if (colisionador.CompareTag("Enemigo"))
            {
                colisionador.transform.GetComponent<Enemigo>().TomarDa�o(da�oGolpe);
            }
            
        }
    }
}
