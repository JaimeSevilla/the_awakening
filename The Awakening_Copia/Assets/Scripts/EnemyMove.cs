using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    public Transform PosJugador;
    public float VelEnemigo;
    private bool FacingRight;

    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        transform.position = Vector2.MoveTowards(transform.position, PosJugador.position, VelEnemigo);
        
    }

 
    
}
