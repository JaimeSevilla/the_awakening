using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueEnemigo : MonoBehaviour
{
    

    [SerializeField] private Transform controladorGolpe;
    [SerializeField] private float radioGolpe;
    [SerializeField] private float da�oGolpe = 1;
    private Animator animator;
    GameObject newTriggerObject;

    private void Start()
    {
        animator = GetComponent<Animator>();
        
    }
    void FixedUpdate()
    {

        ComenzarAtaque();
    }
    


    void OnTriggerEnter(Collider Player)
    {
        
        
    }

    void ComenzarAtaque()
    {
        animator.SetTrigger("Ataque");
        Collider2D[] objetos = Physics2D.OverlapCircleAll(controladorGolpe.position, radioGolpe);

        foreach (Collider2D colisionador in objetos)
        {
            if (colisionador.CompareTag("Player"))
            {
                waiter();
                colisionador.transform.GetComponent<PlayerMovement>().TomarDa�o(da�oGolpe);
            }

        }
    }

    IEnumerator waiter()
    {
        yield return new WaitForSeconds(2);
    }
}
